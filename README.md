Le programme KNNMapReduce permet de construire un modèle KNN qui classe les nouvelles espèces sur la base des mesures des sépales et des pétales de Dataset Iris. Le calcul est décomposé en deux étapes:



*  L'étape de Mapping, qui permet de découper le Dataset Iris en de ligne, de calculer la distance entre chaque ligne et le vecteur d'entrée et de délivrer en sortie un flux textuel, où chaque ligne contient le classe trouvé.




*  L'étape de Reducing, qui permet de calculer l'occurence de chaque classe, pour trouver le classe du vecteur d'entrée.


Pour implémenter ce programme, on se servit de maven qui permet parfaitement les dépendances.
Pour plus de détails sur l'installation, la configuration de Hadoop et l'implémentation de cet exemple de, vous pouvez consulter cet article [http://sysblog.informatique.univ-paris-diderot.fr/?p=2025&preview=true](http://sysblog.informatique.univ-paris-diderot.fr/?p=2025&preview=true)