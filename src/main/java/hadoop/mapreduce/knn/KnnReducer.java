package hadoop.mapreduce.knn;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class KnnReducer extends Reducer<Text, Text, Text, Text> {
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
    {
        HashMap<String,Integer> map=new HashMap<String,Integer>();
        String str=null;
        int maxvalue=-1;

        for(Text value:values)
        {//Si le HashMap map ne contient pas la classe
            if(!map.containsKey(value.toString()))
            {   //On rajoute la classe au HashMap avec un value 1
                map.put(value.toString(),1);
                System.out.println(value.toString());
            }
            //Si la classe existe déjà au niveau de le HashMap map, on incremente la valeur de value
            else
            {

                map.put(value.toString(),map.get(value.toString()+1));
                System.out.println(value.toString());
            }
        }
        System.out.println(map.size());
        Iterator it=map.entrySet().iterator();
        str=((Map.Entry)it.next()).getKey().toString();
        while(it.hasNext())
        {
            Map.Entry entry=(Map.Entry)it.next();
            if(Integer.parseInt(entry.getValue().toString())>maxvalue)
            {
                str=entry.getKey().toString();
                maxvalue=Integer.parseInt(entry.getValue().toString());
            }
        }
        context.write(null,new Text("Class label : "+str));
    }
}


