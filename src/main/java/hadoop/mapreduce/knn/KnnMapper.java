package hadoop.mapreduce.knn;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class KnnMapper extends Mapper<LongWritable, Text, Text, Text> {
    //Cette varaible représente le nombre des attributs sur lequels on va faire l'analyse
    public static int numoffeatures;

    //Le tableau feature représente les attributs feature[i]=0.5, i : le numero de l'attribut, 0.5: la valeur de l'attribut i
    //Aussi il stocke les valeurs du fichier d'input sur lequel on va appliquer l'algorithme KNN
    public static Float[] feature;

    //la list dist représente les résultats de calcul de distance entre les attributs
    public static ArrayList<String> dist=new ArrayList<String>();

    //Methode  pour calculer la distance euclidienne
    public static float euclideandist(Float[] test,int n)
    {
        float distance=0;
        for(int j=0;j<n;j++)
        {
            // feature [5.1 3.5 1.4 0.2]
            distance+=Math.pow((feature[j]-test[j]),2);
        }
        distance=(float)Math.sqrt(distance);
        return distance;
    }

    //Cette méthode permet de lire de paramètres à patir de l'objet context qui permet de personnaliser la logique de traitement
    public void setup(Context context) throws IOException, InterruptedException
    {
        //Initialise la variable numeroffeatures qui représente le nombre des attributs sur lequels on va faire l'analyse
        numoffeatures=context.getConfiguration().getInt("numoffeatures",1);
        feature=new Float[numoffeatures];
        for(int j=0;j<numoffeatures;j++)
        {
            feature[j]=context.getConfiguration().getFloat("feature"+j,0);

        }
    }


    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException
    {
        // la variable value représente le dataSet
        //On fait le split et on stoke le resultat dans le tableau input
        String[] input=value.toString().split(",");
        Float[] test=new Float[numoffeatures];
        for(int j=0;j<numoffeatures;j++)
        {
            test[j]=Float.parseFloat(input[j]);

        }

        //Cette varaible contient le nom de la classe
        String classlabel=input[numoffeatures].replace("\"","");

        // on va ajouter le resultat de la methode euclideandist contacte au nom de la classe à la list dist
        dist.add(String.valueOf(euclideandist(test,numoffeatures))+classlabel);

    }

    //Cette méthode permet de faire un clean up de ressources qui on  a utilisé en générale dans notre cas on va trier la liste dist
    public void cleanup(Context context) throws IOException, InterruptedException
    {
        Collections.sort(dist);
        //Le tableau label contient le cinq premiers classes
        String[] label=new String[5];
        //La variable temp contient les cinq elements les plus proche au niveau de la liste dist, exemple 0.1222setosa
        String temp;
        //On va prendre le cin premiers les plus proches
        for(int j=0;j<5;j++)
        {
            temp=dist.get(j);

            label[j]=String.valueOf(temp.replaceAll("[\\d.]",""));
            context.write(new Text("1"),new Text(label[j]));
        }
    }
}

