package hadoop.mapreduce.knn;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Knn
{

    public static void main(String args[]) throws IOException, InterruptedException, ClassNotFoundException
    {
        // Créer un job en fournissant la configuration et une description textuelle de la tâche
        Configuration conf=new Configuration();
        Job job=new Job(conf,"KNN Classification");

        //On charge le fichier inputIris.txt
        FileSystem hdfs=FileSystem.get(conf);
        BufferedReader br=new BufferedReader(new InputStreamReader(hdfs.open(new Path(args[0]))));
        String line=null;
        int numoffeatures=0;
        while((line=br.readLine())!=null)
        {
            String[] feature=line.split("\\ ");
            numoffeatures=feature.length;
            for(int j=0;j<numoffeatures;j++)
                conf.setFloat("feature"+j,Float.parseFloat(feature[j]));
        }
        br.close();
        hdfs.close();

        //on passe le nombre des attributs
        conf.setInt("numoffeatures",numoffeatures);

        //On definit le main class
        job.setJarByClass(Knn.class);

        //On definit le fichier d'input : le jeu de données Iris, le fichier d'output:le resultat de classification
        FileInputFormat.setInputPaths(job,new Path(args[1]));
        FileOutputFormat.setOutputPath(job,new Path(args[2]));

        //On definit la class Mapper et Reducer
        job.setMapperClass(KnnMapper.class);
        job.setReducerClass(KnnReducer.class);

        //Définition des types clé / valeur de notre problème
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        //On lance le job
        job.waitForCompletion(true);
    }
}
